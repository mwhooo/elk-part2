$urls = @(
    #'https://opendata.ecdc.europa.eu/covid19/vaccine_tracker/csv/data.csv',
    'https://data.rivm.nl/covid-19/COVID-19_aantallen_gemeente_per_dag.csv',
    'https://data.rivm.nl/covid-19/COVID-19_gehandicaptenzorg.csv',
    'https://data.rivm.nl/covid-19/COVID-19_rioolwaterdata.csv',
    'https://data.rivm.nl/covid-19/COVID-19_thuiswonend_70plus.csv',
    'https://data.rivm.nl/covid-19/COVID-19_vaccinatiegraad_per_gemeente_per_week_leeftijd.csv',
    'https://data.rivm.nl/covid-19/COVID-19_verpleeghuizen.csv',
    'https://data.rivm.nl/covid-19/COVID-19_aantallen_gemeente_cumulatief.csv',
    'https://data.rivm.nl/covid-19/COVID-19_uitgevoerde_testen.csv',
    'https://data.rivm.nl/covid-19/COVID-19_ziekenhuisopnames.csv',
    'https://data.rivm.nl/covid-19/COVID-19_varianten.csv',
    'https://data.rivm.nl/covid-19/COVID-19_ziekenhuis_ic_opnames_per_leeftijdsgroep.csv',
    'https://data.rivm.nl/covid-19/COVID-19_ic_opnames.csv',
    'https://data.rivm.nl/covid-19/COVID-19_gedrag.csv',
    'https://data.rivm.nl/covid-19/COVID-19_aantallen_settings_per_dag.csv',
    'https://data.rivm.nl/covid-19/COVID-19_Infectieradar_symptomen_per_dag.csv',
    'https://data.rivm.nl/covid-19/COVID-19_casus_landelijk.csv'
)

if (-not(Get-NetTCPConnection | where LocalPort -eq 9200)){
    #make sure you replace the path tom where youhave Elastic search installed
    start-process cmd.exe -ArgumentList "/c G:\ELK\elasticsearch-7.15.1\bin\elasticsearch.bat"
}

if(-not(Get-NetTCPConnection | where LocalPort -eq 5601)){
    start-process cmd.exe -ArgumentList "/c G:\ELK\kibana-7.15.1\bin\kibana.bat"
}

#make a loop that wait until the ports are opened, and continue from there. easy with powershell.

$CWD = Split-Path $MyInvocation.MyCommand.Definition
Import-Module "$CWD\helpers.psm1" -Force
$logstash_dir = "G:\ELK\logstash-7.15.1\logstash-7.15.1\bin"
$i = 0

foreach ($URL in $urls){
    $filename = Split-Path $URL -Leaf
    $l_file = "$CWD\$filename"
    if (Test-Path $l_file){  Remove-Item $l_file -Force -Confirm:$false }
    Invoke-WebRequest -Uri $URL -OutFile $l_file
    #$newcsv = filtercsv -filepath $l_file -delimiter ";" 
    #$newcsv | Export-Csv $l_file -Force -Delimiter ";" -UseQuotes:Never
    
    $c_file ="$CWD\$filename.conf"
    $p = "$ENV:temp\$i"
    Remove-Item $p -Force -Recurse
    New-Item $p -ItemType Directory -Force
    (Get-Content "$CWD\pipe-template.conf").Replace('__path__replace__me',$l_file.ToLower()).Replace('__replace_index',$filename.ToLower()).replace('\','/') | Out-File $c_file
    start-process cmd.exe -ArgumentList "/c G:\ELK\logstash-7.15.1\logstash-7.15.1\bin\logstash.bat -f $c_file --path.data $p --pipeline.workers 1" -WorkingDirectory $logstash_dir -Wait
    $i++

    new-kibanaindexpattern -indexname $filename
    Remove-Item $c_file -Force -Confirm:$false
}




<#
$body = @'
{
    "index_pattern": {
       "title": "hello"
    }
}
'@

Invoke-WebRequest "http://localhost:5601/api/index_patterns/index_pattern" -Method POST -Headers @{
    "Content-Type" = "application/json"
    "kbn-xsrf" = "true"
} -body $body

#this works on linux sybsystem
#curl -X POST "localhost:5601/api/index_patterns/index_pattern" -H 'kbn-xsrf: true' -H 'Content-Type: application/json' -d $body
#>
