function filtercsv {
    param(
        [Parameter(Mandatory)] [string] $filepath,
        [Parameter(Mandatory)] [string] $delimiter,
        [Parameter(Mandatory=$false)] [string[]] $removed_columns = @('Date_of_report', 'Version') #overwrite when you want with columns to ignore
    )
    #$c_headers = import-csv $l_file -Delimiter ';' | select -first 1 | gm -MemberType Properties | select -ExpandProperty name
    #$removed_columns = @('Date_of_report', 'Version')
    $csv = import-csv $filepath -Delimiter $delimiter | select * -ExcludeProperty $removed_columns
    return($csv)
}

function new-kibanaindexpattern{
    param(
        $indexname
    )

$body = @"
{
    "index_pattern": {
       "title": "$($indexname.tolower())"
    }
}
"@

    Invoke-WebRequest "http://localhost:5601/api/index_patterns/index_pattern" -Method POST -Headers @{
        "Content-Type" = "application/json"
        "kbn-xsrf" = "true"
    } -body $body
}